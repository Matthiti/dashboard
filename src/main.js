import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import http from './services/http';

const app = createApp(App);

app.use(router);

app.config.globalProperties.$axios = http.axios;

app.mount('#app');
