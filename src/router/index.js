import { createRouter, createWebHistory } from 'vue-router';

import http from '@/services/http';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/Login.vue')
    },
    {
      path: '/',
      component: () => import('../layouts/DefaultLayout.vue'),
      children: [
        {
          path: '',
          name: 'dashboard',
          component: () => import('../views/Dashboard.vue')
        },
        {
          path: 'apps',
          name: 'apps',
          component: () => import('../views/Apps.vue')
        },
        {
          path: 'kubernetes',
          name: 'kubernetes',
          component: () => import('../views/kubernetes/Kubernetes.vue')
        },
        {
          path: 'kubernetes/services',
          name: 'kubernetes-services',
          component: () => import('../views/kubernetes/Services.vue')
        },
        {
          path: 'kubernetes/pods',
          name: 'kubernetes-pods',
          component: () => import('../views/kubernetes/Pods.vue')
        },
        {
          path: 'kubernetes/nodes',
          name: 'kubernetes-nodes',
          component: () => import('../views/kubernetes/Nodes.vue')
        },
        {
          path: 'servers',
          name: 'servers',
          component: () => import('../views/Servers.vue')
        }
      ]
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (!http.isLoggedIn() && to.name !== 'login') {
    return next({ name: 'login' });
  }
  next();
});

export default router;
