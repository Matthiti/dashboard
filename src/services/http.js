import axios from 'axios';
import config from '@/config';
import Router from '@/router';

axios.defaults.baseURL = config.apiUrl;

let interceptor;

if (isLoggedIn()) {
  const token = localStorage.getItem('token');
  axios.defaults.headers.common['X-Authorization'] = `Bearer ${token}`;
  enableInterceptor();
}

function enableInterceptor() {
  interceptor = axios.interceptors.response.use(null, error => {
    if (error.response && error.response.status === 401) {
      logout();
    }
    return Promise.reject(error);
  });
}

function isLoggedIn() {
  return localStorage.getItem('token') !== null;
}

function afterLogin(token) {
  localStorage.setItem('token', token);
  axios.defaults.headers.common['X-Authorization'] = `Bearer ${token}`;
  enableInterceptor();
}

function logout() {
  localStorage.removeItem('token');
  axios.interceptors.response.eject(interceptor);
  Router.push({ name: 'login' });
}

export default {
  isLoggedIn,
  afterLogin,
  logout,
  axios
}
